var codes = [];

function generateCode(){
	var nums = [];
	var letters = [];
	do{
		letters[0] = String.fromCharCode(Math.floor(Math.random()*23) + 65);
	} while (letters[0] === 'O' || letters[0] == 'L')

	do{
		letters[1] = String.fromCharCode(Math.floor(Math.random()*23) + 65);
	} while (letters[1] === 'O' || letters[0] == 'L')

	for(var ii=0; ii<10; ii++){
		nums.push(Math.floor(Math.random()*7+2));
	}

	return ''.concat(letters[0], nums[0], nums[1], nums[2], nums[3], nums[4], letters[1], nums[5], nums[6], nums[7], nums[8], nums[9])
}

for (var ii=0; ii < 100; ii++){
	for (var i=0; i<1000; i++){
		do{
			var code = generateCode();
		} while (codes.indexOf(code) !== -1)

		codes.push(code);
		console.log(code + ' ' + i + ' ' + ii);
	}
}

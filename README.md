Groupon Code Generator
===

Simple program I wrote for a client.  They needed to generate thousands of groupon coupon codes.  This simple js script creates 100 sets of 1000 codes in the format of LNNNNNLNNNNN, where L is a letter and N is a number.  Letters cannot be 'O' or 'L'.
